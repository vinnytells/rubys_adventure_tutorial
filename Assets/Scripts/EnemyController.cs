﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public int maxHealth = 3; //Custom: health system similar to main character
    public bool vertical;
    public float speed = 3f;
    public float changeTime = 3.0f;
    public ParticleSystem smokeEffect;
    public float AggroRange = 3.0f; // Custom: Agrgessive range

    Rigidbody2D rb;
    

    float timer;
    int direction = 1;

    public int health { get { return currentHealth; } } //Custom
    int currentHealth; //Custom

    private Transform playerPos; // Custom: Player position
    private Vector2 playerDirection;
    private bool broken = true;
    Animator animator;


    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth; //Custom

        playerPos = FindObjectOfType<RubyController>().transform; //Custom: get Player position        

        //currentHealth = maxHealth;
        rb = GetComponent<Rigidbody2D>();
        timer = changeTime;

        animator = GetComponent<Animator>();
    }



    // Update is called once per frame
    void Update()
    {
        //custom: Get current player orientation/direction
        playerDirection = FindObjectOfType<RubyController>().lookDirection;

        //custom: Follow player if inside range
        float seperation = Vector3.Distance(this.transform.position, playerPos.transform.position);
        
        //Debug.Log("Range to player = " + seperation);
        //Debug.Log(playerPos.position);
        //Debug.Log(playerDirection);

        if ((seperation <= AggroRange) && broken)
        {
            //Debug.Log("Player in range");
            transform.position = Vector2.MoveTowards(transform.position, playerPos.position, speed * Time.deltaTime);

            //custom: verify if player is moving in vertical or not
            if (playerDirection.y != 0f)
                vertical = true;
            else
                vertical = false;

            // custom: verify if player is moving up/down or left/right
            if ((playerDirection.y > 0) || (playerDirection.x > 0))
                direction = 1;
            else
                direction = -1;
        }

        timer -= Time.deltaTime;

        if ((timer < 0) && (seperation > AggroRange))
        {
            direction = -direction;
            timer = changeTime;
        }
        

        //*
        if (!broken)
        {
            return;
        }
        //*/
    }

    void FixedUpdate()
    {        

        Vector2 position = rb.position;

        if (vertical)
        { 
            position.y = position.y + speed * Time.deltaTime * direction;
            animator.SetFloat("Move X", 0);
            animator.SetFloat("Move Y", direction);           
        }
        else
        {
            position.x = position.x + speed * Time.deltaTime * direction;
            animator.SetFloat("Move X", direction);
            animator.SetFloat("Move Y", 0);            
        }

        rb.MovePosition(position);

        //*
        if (!broken)
        {
            return;
        }
        //*/
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        vertical = !vertical; // Custom: if the enemy collides with some object, he changes his orientation
        RubyController player = other.gameObject.GetComponent<RubyController>();

        if (player != null)
        {
            player.ChangeHealth(-1);
        }
    }

    public void Fix()
    {
        broken = false;
        rb.simulated = false;
        animator.SetTrigger("Fixed");
        smokeEffect.Stop();
        //Destroy(smokeEffect.gameObject);
    }

    public void ChangeHealth(int amount)
    {
        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);
        Debug.Log(currentHealth + "/" + maxHealth);
    }
}
