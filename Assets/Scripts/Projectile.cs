﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Projectile : MonoBehaviour
{   

    public float endTime = 5f;
    Rigidbody2D rigidbody2d;
    
   
    void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, endTime);
        /*
        if (transform.position.magnitude > 1000.0f)
        {
            Destroy(gameObject);
        }
        //*/
    }

    public void Launch(Vector2 direction, float force)
    {
        rigidbody2d.AddForce(direction * force);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        EnemyController e = other.collider.GetComponent<EnemyController>();
        if (e != null)
        {
            e.ChangeHealth(-1);
            if (e.health <=0)
                e.Fix();
        }

        //we also add a debug log to know what the projectile touch
        //Debug.Log("Projectile Collision with " + other.gameObject);
        Destroy(gameObject);
    }
}
